package com.xxgy.watchdog.rsp;

import com.xxgy.watchdog.entity.Alarm;

import java.util.List;

public class AlarmListRsp {

    private int count;

    private List<Alarm> alarmList;


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Alarm> getAlarmList() {
        return alarmList;
    }

    public void setAlarmList(List<Alarm> alarmList) {
        this.alarmList = alarmList;
    }
}
