package com.xxgy.watchdog.mapper;

import com.xxgy.watchdog.entity.Alarm;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AlarmMapper {

    @Select("SELECT * FROM alarm WHERE id = #{id}")
    Alarm findAlarmById(@Param("id") String id);

    @Insert("INSERT INTO alarm(id, type, create_time, camera_name, camera_ip, image_path, size, current_dt) VALUES(#{id}, #{type}, #{createTime}, #{cameraName}, #{cameraIp}, #{imagePath}, #{size}, #{currentTime})")
    int insert(Alarm alarm);

    @Select("SELECT id, type, create_time as createTime, camera_name as cameraName, camera_ip as cameraIp, image_path as imagePath, size FROM alarm order by current_dt desc limit #{start}, #{count}")
    List<Alarm> getAlarmList(@Param("start") int start, @Param("count") int count);

    @Select("SELECT count(id) FROM alarm")
    int getAlarmCount();

    @Delete("delete from alarm where id = #{id}")
    int deleteAlarmById(@Param("id") String id);

    @Select("SELECT id, image_path as imagePath from alarm where TO_DAYS(NOW())-TO_DAYS(create_time)>= #{day}")
    List<Alarm> getAlarmListByDay(@Param("day") int day);
}
