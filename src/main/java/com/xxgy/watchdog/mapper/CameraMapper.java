package com.xxgy.watchdog.mapper;

import com.xxgy.watchdog.entity.Alarm;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CameraMapper {
    @Select("SELECT id, type, create_time as createTime, camera_name as cameraName, camera_ip as cameraIp, image_path as imagePath, size FROM alarm where camera_ip = #{camera_ip} and current_dt <= #{end_time} and current_dt >= #{start_time} order by current_dt desc limit #{start}, #{count}")
    List<Alarm> getAlarmListWithTime(@Param("start") int start, @Param("count") int count, @Param("start_time") String startTime, @Param("end_time") String endTime, @Param("camera_ip") String cameraIp);

    @Select("SELECT count(id) FROM alarm where camera_ip = #{camera_ip} and current_dt <= #{end_time} and current_dt >= #{start_time}")
    int getAlarmCountWithTime(@Param("start_time") String startTime, @Param("end_time") String endTime, @Param("camera_ip") String cameraIp);

    @Select("SELECT id, type, create_time as createTime, camera_name as cameraName, camera_ip as cameraIp, image_path as imagePath, size FROM alarm where camera_ip = #{camera_ip} order by current_dt desc limit #{start}, #{count}")
    List<Alarm> getAlarmList(@Param("start") int start, @Param("count") int count, @Param("camera_ip") String cameraIp);

    @Select("SELECT count(id) FROM alarm where camera_ip = #{camera_ip}")
    int getAlarmCount(@Param("camera_ip") String cameraIp);
}
