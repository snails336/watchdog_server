package com.xxgy.watchdog;

import com.xxgy.watchdog.entity.Alarm;
import com.xxgy.watchdog.mapper.AlarmMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

@Component
public class DataDelScheduledService {

    @Autowired
    private AlarmMapper alarmMapper;

    @Scheduled(cron = "0 0/1 * * * ?")
    public void scheduledPerMinute(){
    }


    @Scheduled(cron = "0 0 1 * * ?")
    public void scheduledPerDay() {
        // 每天早上执行
        System.out.print("DataDelScheduledService scheduledPerDay");
        List<Alarm> alarmList = alarmMapper.getAlarmListByDay(30);
        for (Alarm alarm: alarmList) {
            // 先删除数据库，后删除文件
            alarmMapper.deleteAlarmById(alarm.getId());
            String filePath = alarm.getImagePath().replace("http://192.80.5.153:8080/image/", "f:\\watchdog\\");
            File file = new File(filePath);
            if (file.exists()) {
                file.delete();
            }
        }
    }

}
