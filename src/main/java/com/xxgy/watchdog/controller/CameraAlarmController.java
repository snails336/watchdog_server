package com.xxgy.watchdog.controller;

import com.alibaba.fastjson.JSON;
import com.xxgy.watchdog.WebAppConfig;
import com.xxgy.watchdog.entity.Alarm;
import com.xxgy.watchdog.mapper.CameraMapper;
import com.xxgy.watchdog.rsp.AlarmListRsp;
import com.xxgy.watchdog.rsp.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CameraAlarmController {

    @Autowired
    private CameraMapper cameraMapper;

    @RequestMapping(value="/alarm/camera", method= RequestMethod.GET, produces="application/json;charset=UTF-8")
    public String GetAlarmList(@RequestParam("page_count") int pageCount,
                               @RequestParam("page_index") int pageIndex,
                               @RequestParam("camera_ip") String cameraIp,
                               @RequestParam("start_time") String startTime,
                               @RequestParam("end_time") String endTime) {

        int start = pageCount * (pageIndex-1);
        if (startTime == null || endTime == null || startTime.length() == 0 || endTime.length() == 0) {
            int count = cameraMapper.getAlarmCount(cameraIp);
            List<Alarm> alarmList = cameraMapper.getAlarmList(start, pageCount, cameraIp);
            AlarmListRsp rsp = new AlarmListRsp();
            rsp.setCount(count);
            rsp.setAlarmList(alarmList);
            Response<AlarmListRsp> response = new Response<>();
            response.setCode(0);
            response.setMsg("success");
            response.setData(rsp);
            return  JSON.toJSONString(response);
        } else {
            int count = cameraMapper.getAlarmCountWithTime(startTime, endTime, cameraIp);
            List<Alarm> alarmList = cameraMapper.getAlarmListWithTime(start, pageCount, startTime, endTime, cameraIp);
            AlarmListRsp rsp = new AlarmListRsp();
            rsp.setCount(count);
            rsp.setAlarmList(alarmList);
            Response<AlarmListRsp> response = new Response<>();
            response.setCode(0);
            response.setMsg("success");
            response.setData(rsp);
            return  JSON.toJSONString(response);
        }


    }
}
