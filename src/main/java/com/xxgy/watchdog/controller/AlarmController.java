package com.xxgy.watchdog.controller;

import com.alibaba.fastjson.JSON;
import com.xxgy.watchdog.entity.Alarm;
import com.xxgy.watchdog.mapper.AlarmMapper;
import com.xxgy.watchdog.rsp.AlarmListRsp;
import com.xxgy.watchdog.rsp.Response;
import com.xxgy.watchdog.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@RestController
public class AlarmController {

    @Autowired
    private AlarmMapper alarmMapper;


    @RequestMapping(value = "/alarm", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String addAlarm(Alarm alarm) {
        String id = UuidUtils.getUUID();
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = sdf.format(dt);
        alarm.setCurrentTime(currentTime);
        alarm.setId(id);
        if (alarm.getCreateTime() == null) {
            alarm.setCreateTime(currentTime);
        }
        int ret = alarmMapper.insert(alarm);
        Response<Alarm> rsp = new Response<>();
        if (ret == 1) {
            rsp.setCode(0);
            rsp.setMsg("success");
        } else {
            rsp.setCode(-1);
            rsp.setMsg("failed");
        }
        return JSON.toJSONString(rsp);
    }

    @RequestMapping(value = "/alarm", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String GetAlarmList(@RequestParam("page_count") int pageCount,
                               @RequestParam("page_index") int pageIndex) {
        int start = pageCount * (pageIndex - 1);
        int count = alarmMapper.getAlarmCount();
        List<Alarm> alarmList = alarmMapper.getAlarmList(start, pageCount);
        AlarmListRsp rsp = new AlarmListRsp();
        rsp.setCount(count);
        rsp.setAlarmList(alarmList);
        Response<AlarmListRsp> response = new Response<>();
        response.setCode(0);
        response.setMsg("success");
        response.setData(rsp);
        return JSON.toJSONString(response);
    }

    @RequestMapping(value = "/alarm", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
    public String DelAlarm(@RequestParam("id") String id) {
        int ret = alarmMapper.deleteAlarmById(id);
        if (ret > 0) {
            Response<Integer> response = new Response<>();
            response.setCode(0);
            response.setMsg("success");
            response.setData(ret);
            return JSON.toJSONString(response);
        } else {
            Response<Integer> response = new Response<>();
            response.setCode(-1);
            response.setMsg("failed");
            response.setData(ret);
            return JSON.toJSONString(response);
        }
    }
}
