package com.xxgy.watchdog;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;


@Configuration
public class WebAppConfig extends WebMvcConfigurationSupport {

    public static String PATH_PATTERNS = "/image/";

    public static String LOCATION = "file:f:/watchdog/";

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //将网络请求映射到本地路径
        registry.addResourceHandler(PATH_PATTERNS + "**").addResourceLocations(LOCATION);
    }
}
